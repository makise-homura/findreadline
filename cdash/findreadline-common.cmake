cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)
if(NOT CTEST_DROP_METHOD STREQUAL "https")
  set(CTEST_DROP_METHOD "http")
endif()
set(CTEST_DROP_SITE "cdash.ineum.ru")
set(CTEST_DROP_LOCATION "/submit.php?project=FindReadline")
set(CTEST_DROP_SITE_CDASH TRUE)
set(CTEST_NIGHTLY_START_TIME "1:00:00 UTC")
set(CTEST_PROJECT_NAME FindReadline)
set(git_repo "https://gitlab.com/makise-homura/findreadline.git")
set(dashboard_user_home "$ENV{HOME}")
if(NOT DEFINED dashboard_root_name)
  set(dashboard_root_name "My Tests")
endif()
if(NOT DEFINED CTEST_DASHBOARD_ROOT)
  get_filename_component(CTEST_DASHBOARD_ROOT "${CTEST_SCRIPT_DIRECTORY}/../${dashboard_root_name}" ABSOLUTE)
endif()
if(NOT DEFINED dashboard_model)
  set(dashboard_model Nightly)
endif()
if(NOT "${dashboard_model}" MATCHES "^(Nightly|Experimental)$")
  message(FATAL_ERROR "dashboard_model must be Nightly or Experimental")
endif()
if(NOT DEFINED CTEST_BUILD_CONFIGURATION)
  set(CTEST_BUILD_CONFIGURATION Debug)
endif()
if(NOT "${CTEST_CMAKE_GENERATOR}" MATCHES "Make|Ninja")
  # Launchers work only with Makefile and Ninja generators.
  set(CTEST_USE_LAUNCHERS 0)
elseif(NOT DEFINED CTEST_USE_LAUNCHERS)
  set(CTEST_USE_LAUNCHERS 1)
endif()
if(NOT CTEST_TEST_TIMEOUT)
  set(CTEST_TEST_TIMEOUT 1500)
endif()
if(NOT DEFINED git_branch)
  set(git_branch master)
endif()
if(NOT DEFINED dashboard_git_crlf)
  if(UNIX)
    set(dashboard_git_crlf false)
  else(UNIX)
    set(dashboard_git_crlf true)
  endif(UNIX)
endif()
if(NOT DEFINED CTEST_GIT_COMMAND)
  find_program(CTEST_GIT_COMMAND
    NAMES git git.cmd
    PATH_SUFFIXES Git/cmd Git/bin
    )
endif()
if(NOT CTEST_GIT_COMMAND)
  message(FATAL_ERROR "CTEST_GIT_COMMAND not available!")
endif()
execute_process(COMMAND ${CTEST_GIT_COMMAND} --version
                OUTPUT_VARIABLE git_version
                ERROR_QUIET
                OUTPUT_STRIP_TRAILING_WHITESPACE)
if (git_version MATCHES "^git version [0-9]")
  string(REPLACE "git version " "" git_version "${git_version}")
endif()
if("${git_version}" VERSION_LESS 1.6.6)
  message(FATAL_ERROR "CTEST_GIT_COMMAND must be Git >= 1.6.6, not:\n ${git_version}")
endif()
if(NOT DEFINED CTEST_SOURCE_DIRECTORY)
   set(CTEST_SOURCE_DIRECTORY ${CTEST_DASHBOARD_ROOT}/findreadline)
endif()
if(NOT DEFINED CTEST_BINARY_DIRECTORY)
  set(CTEST_BINARY_DIRECTORY ${CTEST_SOURCE_DIRECTORY}-build)
endif()
if("${CTEST_SOURCE_DIRECTORY}" STREQUAL "${CTEST_BINARY_DIRECTORY}")
  message(FATAL_ERROR "In-source testing not supported.  "
    "Specify distinct CTEST_SOURCE_DIRECTORY and CTEST_BINARY_DIRECTORY.")
endif()
macro(dashboard_git)
  execute_process(
    COMMAND ${CTEST_GIT_COMMAND} ${ARGN}
    WORKING_DIRECTORY "${CTEST_SOURCE_DIRECTORY}"
    OUTPUT_VARIABLE dashboard_git_output
    ERROR_VARIABLE dashboard_git_output
    RESULT_VARIABLE dashboard_git_failed
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_STRIP_TRAILING_WHITESPACE
    )
endmacro()
if(EXISTS ${CTEST_SOURCE_DIRECTORY})
  if(NOT EXISTS "${CTEST_SOURCE_DIRECTORY}/.git")
    set(vcs_refresh "because it is not managed by git.")
  else()
    dashboard_git(reset --hard)
    if(dashboard_git_failed)
      set(vcs_refresh "because its .git may be corrupted.")
    endif()
  endif()
  if(vcs_refresh AND "${CTEST_SOURCE_DIRECTORY}" MATCHES "/findreadline[^/]*")
    message("Deleting source tree\n  ${CTEST_SOURCE_DIRECTORY}\n${vcs_refresh}")
    file(REMOVE_RECURSE "${CTEST_SOURCE_DIRECTORY}")
  endif()
endif()
if(NOT EXISTS "${CTEST_SOURCE_DIRECTORY}"
    AND NOT DEFINED CTEST_CHECKOUT_COMMAND)
  # Generate an initial checkout script.
  get_filename_component(_name "${CTEST_SOURCE_DIRECTORY}" NAME)
  set(ctest_checkout_script ${CTEST_DASHBOARD_ROOT}/${_name}-init.cmake)
  file(WRITE ${ctest_checkout_script} "# git repo init script for ${_name}
execute_process(
  COMMAND \"${CTEST_GIT_COMMAND}\" clone -n -- \"${git_repo}\"
          \"${CTEST_SOURCE_DIRECTORY}\"
  )
if(EXISTS \"${CTEST_SOURCE_DIRECTORY}/.git\")
  execute_process(
    COMMAND \"${CTEST_GIT_COMMAND}\" config core.autocrlf ${dashboard_git_crlf}
    WORKING_DIRECTORY \"${CTEST_SOURCE_DIRECTORY}\"
    )
  execute_process(
    COMMAND \"${CTEST_GIT_COMMAND}\" fetch
    WORKING_DIRECTORY \"${CTEST_SOURCE_DIRECTORY}\"
    )
  execute_process(
    COMMAND \"${CTEST_GIT_COMMAND}\" checkout ${git_branch}
    WORKING_DIRECTORY \"${CTEST_SOURCE_DIRECTORY}\"
    )
endif()
")
  set(CTEST_CHECKOUT_COMMAND "\"${CMAKE_COMMAND}\" -P \"${ctest_checkout_script}\"")
elseif(EXISTS "${CTEST_SOURCE_DIRECTORY}/.git")
  # Upstream URL.
  dashboard_git(config --get remote.origin.url)
  if(NOT dashboard_git_output STREQUAL "${git_repo}")
    dashboard_git(config remote.origin.url "${git_repo}")
  endif()
  # Local checkout.
  dashboard_git(symbolic-ref HEAD)
  if(NOT dashboard_git_output STREQUAL "${git_branch}")
    dashboard_git(checkout ${git_branch})
    if(dashboard_git_failed)
      message(FATAL_ERROR "Failed to checkout branch ${git_branch}:\n${dashboard_git_output}")
    endif()
  endif()
endif()
unset(CTEST_CONFIGURE_COMMAND)
list(APPEND CTEST_NOTES_FILES
  "${CTEST_SCRIPT_DIRECTORY}/${CTEST_SCRIPT_NAME}"
  "${CMAKE_CURRENT_LIST_FILE}"
  )
foreach(req
    CTEST_CMAKE_GENERATOR
    CTEST_SITE
    CTEST_BUILD_NAME
    )
  if(NOT DEFINED ${req})
    message(FATAL_ERROR "The containing script must set ${req}")
  endif()
endforeach(req)
set(vars "")
foreach(v
    CTEST_SITE
    CTEST_BUILD_NAME
    CTEST_SOURCE_DIRECTORY
    CTEST_BINARY_DIRECTORY
    CTEST_CMAKE_GENERATOR
    CTEST_BUILD_CONFIGURATION
    CTEST_GIT_COMMAND
    CTEST_CHECKOUT_COMMAND
    CTEST_SCRIPT_DIRECTORY
    CTEST_USE_LAUNCHERS
    )
  set(vars "${vars}  ${v}=[${${v}}]\n")
endforeach(v)
message("Dashboard script configuration:\n${vars}\n")
set(ENV{LC_ALL} C)
macro(write_cache)
  set(cache_build_type "")
  set(cache_make_program "")
  if(CTEST_CMAKE_GENERATOR MATCHES "Make|Ninja")
    set(cache_build_type CMAKE_BUILD_TYPE:STRING=${CTEST_BUILD_CONFIGURATION})
    if(CMAKE_MAKE_PROGRAM)
      set(cache_make_program CMAKE_MAKE_PROGRAM:FILEPATH=${CMAKE_MAKE_PROGRAM})
    endif()
  endif()
  set(cache_git_executable "")
  file(WRITE ${CTEST_BINARY_DIRECTORY}/CMakeCache.txt "
SITE:STRING=${CTEST_SITE}
BUILDNAME:STRING=${CTEST_BUILD_NAME}
CTEST_USE_LAUNCHERS:BOOL=${CTEST_USE_LAUNCHERS}
DART_TESTING_TIMEOUT:STRING=${CTEST_TEST_TIMEOUT}
${cache_build_type}
${cache_make_program}
")
endmacro(write_cache)
macro(dashboard_run)
  set(ENV{HOME} "${dashboard_user_home}")
  message("Clearing build tree...")
  ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
  ctest_start(${dashboard_model})
  write_cache()
  ctest_update(RETURN_VALUE count)
  set(CTEST_CHECKOUT_COMMAND) # checkout on first call only
  message("Found ${count} changed files")
  ctest_configure(OPTIONS ${CTEST_CONFIG_OPTIONS})
  ctest_read_custom_files(${CTEST_BINARY_DIRECTORY})
  ctest_build()
  ctest_test(${CTEST_TEST_ARGS})
  if(dashboard_do_coverage)
    ctest_coverage()
  endif()
  if(dashboard_do_memcheck)
    ctest_memcheck()
  endif()
  if(NOT dashboard_no_submit)
    ctest_submit()
  endif()
endmacro()
dashboard_run()
set(dashboard_model Experimental)
set(dashboard_build_name ${CTEST_BUILD_NAME})
dashboard_git(branch -f experimental)
dashboard_git(checkout experimental)
dashboard_git(fetch origin --prune "+refs/test-topics/*:refs/test-topics/*")
set(topic_names "")
dashboard_git(for-each-ref refs/test-topics/ "--format=%(refname)")
string(REPLACE "\n" ";" topic_lines "${dashboard_git_output}")
foreach(line ${topic_lines})
  if("${line}" MATCHES "^refs/test-topics/([A-Za-z0-9/._-]+)$")
    list(APPEND topic_names ${CMAKE_MATCH_1})
  else()
    message(WARNING "Skipping unrecognized line:\n ${line}\n")
  endif()
endforeach()
foreach(topic ${topic_names})
  message(STATUS "Testing topic '${topic}'")
  set(topic_ref refs/test-topics/${topic})
  dashboard_git(reset --hard ${git_branch})
  dashboard_git(rev-parse --verify -q --short=5 ${topic_ref})
  if(dashboard_git_failed)
    message(WARNING "Skipping topic '${topic}' that disappeared!")
  else()
    set(short ${dashboard_git_output})
    set(CTEST_BUILD_NAME ${topic}-g${short}+${dashboard_build_name})
    set(CTEST_GIT_UPDATE_CUSTOM ${CTEST_GIT_COMMAND} reset --hard ${topic_ref})
    dashboard_run()
  endif()
endforeach()
set(ENV{HOME} "${dashboard_user_home}")
