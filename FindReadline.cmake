# Distributed under the MIT License.

#[=======================================================================[.rst:
FindReadline
------------

Finds the Readline library.

Input Variables
^^^^^^^^^^^^^^^

The following variables will alter the behavior of search:

``Readline_INSTALL_PATH``
  Suggested directory where Readline is installed.
``Readline_INCLUDE_DIRS``
  Suggested include directories needed to use Readline.
``Readline_LIBRARY_DIRS``
  Suggested library directories needed to use Readline.
``Readline_DONT_SEARCH_MSYS``
  On ``WIN32``, don't try to search readline in MSYS.
``Readline_DONT_SEARCH_GW32``
  On ``WIN32``, don't try to search readline in GnuWin32.
``Readline_DONT_SEARCH_TRIPLE``
  On ``UNIX``, don't try to search readline in tpiple-specific directories.
``MSYS_INSTALL_PATH``
  On ``WIN32``, explicitly set MSYS installation path.
``MSYS_COMPILER_DIR``
  MSYS compiler used (``mingw32``, ``mingw64``, ``clang32``, ``clang64``).
``GW32_INSTALL_PATH``
  On ``WIN32``, explicitly set GnuWin32 installation path.

Search order
^^^^^^^^^^^^

First: ``Readline_INCLUDE_DIRS`` and ``Readline_LIBRARY_DIRS``, if specified.
Second: ``Readline_INSTALL_PATH``, if specified.
Third: pkg-config, if found.
Fourth: MSYS, if not disabled (only on Windows).
Fifth: GnuWin32, if not disabled (only on Windows).
Sixth: triplet library and include subdirectories, if not disabled (only on Unix).
Seventh: general library and include subdirectories (only on Unix).

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

``Readline_FOUND``
  True if the system has the Readline library.
``Readline_INCLUDE_DIRS``
  Include directories needed to use Readline.
``Readline_LIBRARY_DIRS``
  Library directories needed to use Readline.
``Readline_LIBRARIES``
  Libraries needed to link to Readline.
``Readline_VERSION``
  The version of the Readline library which was found, if available.

#]=======================================================================]

# Predefined directories
if(Readline_INSTALL_PATH)
    cmake_path(APPEND Readline_INSTALL_PATH "include" OUTPUT_VARIABLE PD_Readline_INCLUDE_DIRS)
    cmake_path(APPEND Readline_INSTALL_PATH "lib" OUTPUT_VARIABLE PD_Readline_LIBRARY_DIRS)
endif()

# First approach: pkg-config
find_package(PkgConfig)
if("${PkgConfig_FOUND}")
    pkg_check_modules(PC_Readline "readline")
endif()

# Second approach: MSYS
if(WIN32 AND NOT Readline_DONT_SEARCH_MSYS)
    if(NOT MSYS_INSTALL_PATH)
        find_package(Msys)
    endif()
    if(NOT MSYS_COMPILER_DIR)
        if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
            set(MSYS_COMPILER_DIR "mingw64")
        else()
            set(MSYS_COMPILER_DIR "mingw32")
        endif()
    endif()
    if(MSYS_INSTALL_PATH)
        cmake_path(APPEND MSYS_INSTALL_PATH "${MSYS_COMPILER_DIR}" "include" OUTPUT_VARIABLE MS_Readline_INCLUDE_DIRS)
        cmake_path(APPEND MSYS_INSTALL_PATH "${MSYS_COMPILER_DIR}" "lib" OUTPUT_VARIABLE MS_Readline_LIBRARY_DIRS)
    endif()
endif()

# Third approach: GnuWin32
if(WIN32 AND NOT Readline_DONT_SEARCH_GW32)
    if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
        set(GW32_VIEW 64)
    else()
        set(GW32_VIEW 32)
    endif()
    cmake_host_system_information(RESULT GW32_Readline_INSTDIR QUERY WINDOWS_REGISTRY "HKEY_LOCAL_MACHINE\\SOFTWARE\\GnuWin32\\Readline" VALUE "InstallPath" VIEW ${GW32_VIEW} ERROR_VARIABLE GW32_Readline_INSTDIR_ERR)
    if(NOT GW32_Readline_INSTDIR_ERR)
        cmake_path(APPEND GW32_Readline_INSTDIR "include" OUTPUT_VARIABLE GW_Readline_INCLUDE_DIRS)
        cmake_path(APPEND GW32_Readline_INSTDIR "lib" OUTPUT_VARIABLE GW_Readline_LIBRARY_DIRS)
    endif()
endif()

# Fallback: default UNIX directories
if(UNIX)
    if(NOT Readline_DONT_SEARCH_TRIPLE)
        get_property(LANGS GLOBAL PROPERTY ENABLED_LANGUAGES)
        if("CXX" IN_LIST LANGS)
            set(CHECK_TRIPLE_CMD "${CMAKE_CXX_COMPILER}")
        elseif("C" IN_LIST LANGS)
            set(CHECK_TRIPLE_CMD "${CMAKE_C_COMPILER}")
        endif()
        if(CHECK_TRIPLE_CMD)
            execute_process(COMMAND "${CHECK_TRIPLE_CMD}" --print-multiarch OUTPUT_VARIABLE SYSTEM_TRIPLE ERROR_QUIET RESULT_VARIABLE SYSTEM_TRIPLE_VALID OUTPUT_STRIP_TRAILING_WHITESPACE)
            if(SYSTEM_TRIPLE_VALID EQUAL 0)
                set(UX_Readline_INCLUDE_DIRS /usr/local/include/${SYSTEM_TRIPLE} /usr/include/${SYSTEM_TRIPLE})
                set(UX_Readline_LIBRARY_DIRS /usr/local/lib/${SYSTEM_TRIPLE} /usr/lib/${SYSTEM_TRIPLE})
            endif()
        endif()
    endif()
    set(UX_Readline_INCLUDE_DIRS ${UX_Readline_INCLUDE_DIRS} /usr/local/include /usr/include)
    set(UX_Readline_LIBRARY_DIRS ${UX_Readline_LIBRARY_DIRS} /usr/local/lib /usr/lib)
endif()

message(DEBUG "FindReadline results:")
message(DEBUG " Include dirs:")
message(DEBUG "  - directly specified: ${Readline_INCLUDE_DIRS}")
message(DEBUG "  - by install path:    ${PD_Readline_INCLUDE_DIRS}")
message(DEBUG "  - by pkg-config:      ${PC_Readline_INCLUDE_DIRS}")
message(DEBUG "  - by MSYS:            ${MS_Readline_INCLUDE_DIRS}")
message(DEBUG "  - by GnuWin32:        ${GW_Readline_INCLUDE_DIRS}")
message(DEBUG "  - UNIX fallback:      ${UX_Readline_INCLUDE_DIRS}")
message(DEBUG " Libraries:")
message(DEBUG "  - directly specified: ${Readline_LIBRARY_DIRS}")
message(DEBUG "  - by install path:    ${PD_Readline_LIBRARY_DIRS}")
message(DEBUG "  - by pkg-config:      ${PC_Readline_LIBRARY_DIRS}")
message(DEBUG "  - by MSYS:            ${MS_Readline_LIBRARY_DIRS}")
message(DEBUG "  - by GnuWin32:        ${GW_Readline_LIBRARY_DIRS}")
message(DEBUG "  - UNIX fallback:      ${UX_Readline_LIBRARY_DIRS}")

find_path(Readline_INCLUDE_DIR NAMES readline/readline.h PATHS ${Readline_INCLUDE_DIRS} ${PD_Readline_INCLUDE_DIRS} ${PC_Readline_INCLUDE_DIRS} ${MS_Readline_INCLUDE_DIRS} ${GW_Readline_INCLUDE_DIRS} ${UX_Readline_INCLUDE_DIRS} NO_DEFAULT_PATH)
find_library(Readline_LIBRARY NAMES readline PATHS ${Readline_LIBRARY_DIRS} ${PD_Readline_LIBRARY_DIRS} ${PC_Readline_LIBRARY_DIRS} ${MS_Readline_LIBRARY_DIRS} ${GW_Readline_LIBRARY_DIRS} ${UX_Readline_LIBRARY_DIRS} NO_DEFAULT_PATH)
set(Readline_VERSION ${PC_Readline_VERSION})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Readline REQUIRED_VARS Readline_LIBRARY Readline_INCLUDE_DIR VERSION_VAR Readline_VERSION)

message(DEBUG " Results:")
message(DEBUG "  - include dir:   ${Readline_INCLUDE_DIR}")
message(DEBUG "  - library:       ${Readline_LIBRARY}")

if(Readline_FOUND)
    set(Readline_LIBRARIES "readline")
    set(Readline_INCLUDE_DIRS "${Readline_INCLUDE_DIR}")
    get_filename_component(Readline_LIBRARY_DIRS ${Readline_LIBRARY} DIRECTORY)
endif()
