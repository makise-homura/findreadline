#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>

int main()
{
    char *line = readline ("Enter a line: ");
    printf("You entered: '%s'\n", line);
    free(line);
}
