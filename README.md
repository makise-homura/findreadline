# FindReadline

FindReadline module for CMake >= 3.20.

## Features

* Supports searching in POSIX directories (e.g. `/usr/include`, `/usr/lib`)
* Supports searching in triple-marked directories (e.g. `/usr/include/x86_64-linux-gnu`, `/usr/lib/x86_64-linux-gnu`); can be disabled
* Supports searching in MSYS installation under Windows; can be disabled
* Supports searching in GnuWin32 installation under Windows; can be disabled
* Supports searching in predefined installation directory (e.g. `/opt/readline`, `c:/gnuwin32/readline`)
* Supports searching in predefined include and library directory (e.g. `/opt/readline/headers` and `/opt/readline/libraries`)
* MSYS directory and compiler can be predefined rather than found using CMake `FindMsys` module
* GnuWin32 directory can be predefined rather than found using registry

## Usage

Consider readline is needed for target `<TARGET>`, and you have `FindReadline.cmake` at `<MOD_DIR>` (usually `${CMAKE_CURRENT_LIST_DIR}`).

Add this to your `CMakeLists.txt`:

```
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" <MOD_DIR>)
find_package(Readline REQUIRED)
if(Readline_FOUND)
    target_include_directories(<TARGET> PRIVATE ${Readline_INCLUDE_DIRS})
    target_link_directories(<TARGET> PRIVATE ${Readline_LIBRARY_DIRS})
    target_link_libraries(<TARGET> ${Readline_LIBRARIES})
endif()

```

or, if readline is not absolutely required, and you are using flag like `USE_READLINE` to found out if it is usable,

```
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" <MOD_DIR>)
find_package(Readline)
if(Readline_FOUND)
    target_compile_options(<TARGET> PRIVATE "-DUSE_READLINE=1")
    target_include_directories(<TARGET> PRIVATE ${Readline_INCLUDE_DIRS})
    target_link_directories(<TARGET> PRIVATE ${Readline_LIBRARY_DIRS})
    target_link_libraries(<TARGET> ${Readline_LIBRARIES})
endif()
```

### Input CMake variables

* `Readline_INSTALL_PATH`: Suggested directory where Readline is installed.
* `Readline_INCLUDE_DIRS`: Suggested include directories needed to use Readline.
* `Readline_LIBRARY_DIRS`: Suggested library directories needed to use Readline.
* `Readline_DONT_SEARCH_MSYS`: On Windows, don't try to search readline in MSYS.
* `Readline_DONT_SEARCH_GW32`: On Windows, don't try to search readline in GnuWin32.
* `Readline_DONT_SEARCH_TRIPLE`: On POSIX, don't try to search readline in tpiple-specific directories.
* `MSYS_INSTALL_PATH`: On Windows, explicitly set MSYS installation path.
* `MSYS_COMPILER_DIR`: MSYS compiler used (`mingw32`, `mingw64`, `clang32`, `clang64`).
* `GW32_INSTALL_PATH`: On Windows, explicitly set GnuWin32 installation path.

### Search order

* `Readline_INCLUDE_DIRS` and `Readline_LIBRARY_DIRS`, if specified.
* `Readline_INSTALL_PATH`, if specified.
* `pkg-config`, if found.
* MSYS, if not disabled (only on Windows).
* GnuWin32, if not disabled (only on Windows).
* triple-marked library and include subdirectories, if not disabled (only on Unix).
* POSIX library and include subdirectories (only on Unix).

### Output CMake variables

* `Readline_FOUND`: True if the system has the Readline library.
* `Readline_INCLUDE_DIRS`: Include directories needed to use Readline.
* `Readline_LIBRARY_DIRS`: Library directories needed to use Readline.
* `Readline_LIBRARIES`: Libraries needed to link to Readline.
* `Readline_VERSION`: The version of the Readline library which was found, if available.

## Windows notes

The fastest way to use Readline on Windows is using MSYS and its corresponding package:
```
wget https://github.com/msys2/msys2-installer/releases/download/2021-07-25/msys2-x86_64-20210725.exe
msys2-x86_64-20210725.exe
set PATH=%PATH%;c:\msys64\usr\bin
pacman -S mingw-w64-x86_64-readline
```

Don't forget to add path to `libreadline8.dll` (e.g. `c:\msys64\mingw64\bin`) or so to your `PATH`, or you won't be able to run dynamically linked binaries.

## CDash testing

### Linux

Install all prerequistes needed for build, and also git and vncserver.

```
sudo useradd -d /var/ci_user -m ci_user
sudo su - ci_user
git clone https://gitlab.com/makise-homura/findreadline.git
cd findreadline/cdash
ln -s run_tests.linux run_tests
exit
sudo cp ~ci_user/findreadline/cdash/findreadline-testing.cron /etc/cron.d/findreadline-testing
```

Edit `~ci_user/findreadline/cdash/run_tests.linux` to change proxy addresses, if needed.

Edit `~ci_user/findreadline/cdash/local.cmake` to set `CTEST_SITE` and `CTEST_BUILD_NAME`.

You may set `CTEST_CONFIG_OPTIONS` if you need, e.g. you may specify something like `"-DCMAKE_CXX_COMPILER=g++-7.3"` to change C++ compiler.

Test it by running `sudo -i -u ci_user /var/ci_user/findreadline/cdash/run_tests`.

### Windows

Install all prerequistes needed for build, and also git.

Check git, CMake/CTest, and gcc/g++ are in your system-wide PATH. Note the following-like paths:
```
C:\Program Files\Git\cmd;
C:\Program Files\CMake\bin;
```

Create a user account for testing and log in under it.

Clone the repository:

```
git clone https://gitlab.com/makise-homura/findreadline.git
```

Edit `findreadline/cdash/run_tests.windows.bat` to change proxy addresses, if needed.

Edit `findreadline/cdash/env.cmake` to define path to your make utility.

Edit `findreadline/cdash/local.cmake` to set `CTEST_SITE` and `CTEST_BUILD_NAME`.

Create a task to run, e.g., each day at 4:00 AM that executes `run_tests.windows.bat` with current directory of `findreadline/cdash` relative to user's home directory.

Test it by running `run_tests.windows.bat` from `findreadline/cdash`.

If you get an upload error like this:
```
schannel: next InitializeSecurityContext failed: Unknown error (0x80092013)
```
Try adding the following line:
```
set(CTEST_CURL_OPTIONS "CURLOPT_SSL_VERIFYPEER_OFF;CURLOPT_SSL_VERIFYHOST_OFF")
```
before all `set()` expressions in `findreadline/cdash/local.cmake`.

If test fails with error like:
```
execute_process abnormal exit: Exit code 0xc0000135
```
check if you have installed readline correctly, e.g. you must have `readline8.dll` available in `PATH`.